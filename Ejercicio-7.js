db.getCollection("vehiculo").insertMany([
                                                {
                                                    "Marca":"Ford",
                                                    "Modelo": "Explorer",
                                                    "Precio": 474350.80,
                                                    "Color": {
                                                            "C1": "Rojo",
                                                            "C2": "Amarillo"
                                                        }
                                                },
                                                {
                                                    "Marca":"Audi",
                                                    "Modelo": "Q8",
                                                    "Precio": 574325.19,
                                                    "Color": {
                                                            "C1": "Rojo",
                                                            "C2": "Azul"
                                                        }
                                                },
                                                {
                                                    "Marca":"Ford",
                                                    "Modelo": "Fiesta",
                                                    "Precio": 105859.62,
                                                    "Color": {
                                                            "C1": "Azul",
                                                            "C2": "Blanco"
                                                        }
                                                },
                                                {
                                                    "Marca":"Ford",
                                                    "Modelo": "Mustang",
                                                    "Precio": 40301789.50,
                                                    "Color": {
                                                            "C1": "Plomo",
                                                            "C2": "Azul"
                                                        }
                                                },
                                                {
                                                    "Marca":"Bentley",
                                                    "Modelo": "Continental GT",
                                                    "Precio": 958264.26,
                                                    "Color": {
                                                            "C1": "Morado",
                                                            "C2": "Plomo",
                                                            "C3": "Negro"
                                                        }
                                                },
                                                {
                                                    "Marca":"BMW",
                                                    "Modelo": "Serie 3",
                                                    "Precio": 462598.26,
                                                    "Color": {
                                                            "C1": "Azul",
                                                            "C2": "Rojo",
                                                            "C3": "Plomo"
                                                        }
                                                },
                                                {
                                                    "Marca":"Ford",
                                                    "Modelo": "Ranger",
                                                    "Precio": 210597.30,
                                                    "Color": {
                                                            "C1": "Rojo",
                                                            "C2": "Naranja"
                                                        }
                                                },
                                                {
                                                    "Marca":"Audi",
                                                    "Modelo": "Q3",
                                                    "Precio": 423598.33,
                                                    "Color": {
                                                            "C1": "Escarlata",
                                                            "C2": "Negro"
                                                        }
                                                }
                                            ])
                                                


                         
db.getCollection("vehiculo").createIndex({"Marca":1})
db.getCollection("vehiculo").createIndex({"Color": 1})

/* BUSCANDO POR LA MARCA  */
db.getCollection("vehiculo").find({"Marca": "Ford"})

/*  DATOS GENERALES  */
db.getCollection("vehiculo").find({})
